# Nbt

``` python

from nbt import Node, print_tree, get_parents
    
root = Node(value="ROOT")

a1 = Node(value="a", parent = root)
b1 = Node(value="b", parent = root)
c1 = Node(value="c", parent = root)

a2 = Node(value="aa1", parent = a1)
a3 = Node(value="aa2", parent = a1)
b2 = Node(value="bb1", parent = b1)

a4 = Node(value="aaa1", parent = a2)
a5 = Node(value="aaa2", parent = a3)

print("full tree structure")
print_tree(root)
print()

n = a5
print(f"ancestry of the node {n}")
x = get_parents(n)
print(x)


```

